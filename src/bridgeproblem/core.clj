(ns bridgeproblem.core
  (:gen-class))

(use 'bridgeproblem.bridge-state-representation 'algo.backtrack)

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (doall (map #(prn (:current-operator %) (-> % :state :torch-position )) (backtrack start-state apply-operator all-possible-operator operator-price is-goal))))
