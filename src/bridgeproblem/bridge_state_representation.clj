;; ## The Bridge and torch problem (logic puzzle)
;;
;; There is bridge reached by 4 people. The bridge is so narrow it can
;; only hold two people at a time. It is late at night so without the
;; torch the bridge cannot be crossed. 
;;
;; The speed of the men are the following for crossing the bridge:
;; 1 minute, 2, 5 and 10 minutes.
;;
;; What is the minimum time for crossing the bridge by all the four men?
(ns bridgeproblem.bridge-state-representation)

(use 'clojure.set)

;; State describes the who are on the two sides of the bridge 
;; and on which sides the torch is.
(defrecord State [left-side right-side torch-position])

;; Operator is set containing the men to move. 
;; Where a man is represented by the number equal how long does 
;; he take to reach the other side.
(defrecord Operator [men])

(def start-state
  "The start state. 
  Everybody is on the left-side and the torch is also wit them."
  (->State #{1 2 5 10} #{} :left-side))

(defn- choose2 
  "Helper function: returns all the possible subsets of twos as a list"
  [coll]
  (for [i coll j (disj coll i)] #{i j}))

(defn- choose1 
  "Helper function: Returns all the possible subsets of ones as a list"
  [coll]
  (for [i coll] #{i}))

(defn all-possible-leaving 
  "Returns all the possible leaving mens of as a list of sets"
  [coll]
  (union (choose1 coll) (choose2 coll)))

(defn one-side-operators 
  "Returns all the possible operators from a given side"
  [state side]
  (map #(->Operator %) (all-possible-leaving (side state))))

(defn all-possible-operator 
  "Returns all the possible operators from a state"
  [state]
  (case (:torch-position state)
    :left-side (one-side-operators state :left-side)
    :right-side (one-side-operators state :right-side)))

(defn operator-price 
  "Returns the price of the operator"
  [operator]
  (apply max (:men operator)))


(defn apply-operator 
  "Returns the new state after the given operator is executed on
  the initial state."
  [initial-state operator]
  (let [{left :left-side right :right-side torch-position :torch-position} initial-state
        {men :men} operator]
    (case torch-position
      :left-side (->State
                   (clojure.set/difference (:left-side initial-state) men)
                   (clojure.set/union (:right-side initial-state) men)
                   :right-side)
      :right-side (->State
                    (clojure.set/union (:left-side initial-state) men)
                    (clojure.set/difference (:right-side initial-state) men)
                    :left-side))))


(defn is-goal 
  "Returns true iff the state is the goal"
  [state]
  (empty? (:left-side state)))



