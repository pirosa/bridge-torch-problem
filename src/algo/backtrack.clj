;; ## Implementation of the backtrack algorithm
;;
;; http://en.wikipedia.org/wiki/Backtracking
;;
(ns algo.backtrack)

;; Backtrack-State is used by backtrack algorithm. 
;; state: the wrapped original state
;; price: the price up to now
;; current-operator: it is used for to read out the final solution as a list of of operators from the start state
;; avail-operators: the remaining availabale operator in this state
(defrecord Backtrack-State [state price current-operator avail-operators])

(defn is-circle 
  "Returns true iff the given state already covered in stack"
  [state-stack new-state]
  (some #(= new-state (:state %)) state-stack))

(defn backtrack 
  "The backtrack implementation"
  [start-state-data apply-operator-fn all-possible-operator-fn operator-price-fn is-goal?]
  (loop [best-price Integer/MAX_VALUE
         state-stack (list (->Backtrack-State start-state-data 0 nil (all-possible-operator-fn start-state-data)))
         solution-stack '()]
    (if (empty? state-stack) solution-stack
      (let [[{state :state price :price  avail-operators :avail-operators} & rest-states]  state-stack]
        (if (>= price best-price) (recur
                                    best-price
                                    rest-states
                                    solution-stack)
          (if (is-goal? state) (recur
                                   (min best-price price)
                                   rest-states
                                   state-stack)
            (if (empty? avail-operators) (recur
                                           best-price
                                           rest-states
                                           solution-stack)
              (let [[operator-to-apply & rest-operators] avail-operators]
                (let [modified-stack (conj rest-states (->Backtrack-State state price operator-to-apply rest-operators))
                      new-state (apply-operator-fn state operator-to-apply)]
                  (if (is-circle state-stack new-state) (recur
                                                          best-price
                                                          modified-stack
                                                          solution-stack)
                    (recur
                      best-price
                      (conj
                        modified-stack
                        (->Backtrack-State new-state (+ price (operator-price-fn operator-to-apply)) nil (all-possible-operator-fn new-state)))
                      solution-stack)))))))))))

